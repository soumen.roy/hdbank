# HDBank


HDBank library is designed to construct template banks for searching gravitational waves from compact binary coalescences using the geometric-random template placement algorithm. The current version of this code can generate the template bank for non-precessing binary systems with (2, 2) mode waveform. The details of the method are:
*  S. Roy, A. S. Sengupta, and N. Thakor, Phys. Rev. D95, 104045 (2017), [1702.06771](https://arxiv.org/abs/1702.06771)
*  S. Roy, A. S. Sengupta,  and P. Ajith, Phys. Rev. D99, 024048 (2019), [1711.08743](https://arxiv.org/abs/1711.08743)




## Installation
One key component for generating the hybrid bank is metric over the dimensionless chirp time coordinate. To compute the metric, we modified in the LALSimulation code base. So, we first create a conda environment and install LALSuite.

### Simple installation
```sh
git clone https://git.ligo.org/soumen.roy/hdbank.git
cd hdbank
sh install.sh
```

### Install step-by-step
1. Clone hdbank repository
  ```sh
  git clone https://git.ligo.org/soumen.roy/hdbank.git
  cd hdbank
  ```
2. Clone LALSuite repository
```sh
git clone https://git.ligo.org/lscsoft/lalsuite.git
cd lalsuite

git checkout lalsuite-v7.21
cp ../modify.patch

git apply modify.patch
```
3. Create conda environment and install LALSuite 
```sh
conda env create --name hdbank-dev -f common/conda/environment.yml
conda activate hdbank-dev
./00boot
./configure --prefix=$CONDA_PREFIX CFLAGS="-Wno-error" --enable-swig-python --disable-all-lal  --enable-lalsimulation --enable-lalinspiral --enable-lalframe --enable-lalmetaio --enable-lalburst
make -j 4 && make install
cd ..
```
4. Install hdbank
`pip3 install .`


## Workflow of placement algorithm

<img src="/docs/algo.png"  width="400">
