export OPENBLAS_MAIN_FREE=1
export OPENBLAS_NUM_THREADS=1

hdbank --approximant IMRPhenomXAS \
            --min-mass1 10.0 --max-mass1 100.0 \
            --min-mass2 10.0 --max-mass2 100.0  \
            --min-bh-spin -0.99 --max-bh-spin 0.99 \
            --min-match 0.97 \
            --flow 20.0 --fhigh 1024.0 --df 0.1 \
            --psd-model aLIGOZeroDetHighPower \
            --random-list-size 500000 \
            --template-bin-size 1000 \
            --number-threads 16 \
            --output-filename BBH_HYBRID_BANK.xml.gz \
            --write-metric \
            --optimize-A3star-lattice-orientation \
            --enable-exact-match \
            --verbose