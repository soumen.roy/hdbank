import os
os.environ["MKL_NUM_THREADS"] = "1" 
os.environ["NUMEXPR_NUM_THREADS"] = "1" 
os.environ["OMP_NUM_THREADS"] = "1" 

import numpy as np
import lal
import lalsimulation as lalsim
from hdbank import metric
import timeit

def compute_time_for_metric_computation():
    gm = metric.ComputeNumericalIMRMetric(fref, flow, fhigh, df, ASD, approximant=approx)
    return gm.Theta0Theta3Theta3s(**params_dic)



def gen_waveform():
    approximant = lalsim.GetApproximantFromString( approx )
    extra_params = None
    mass1 = 20.0
    mass2 = 10.0
    spin1z = 0.5
    spin2z=0.5
    if approx == 'SEOBNRv5_ROM':
        extra_params = lal.CreateDict()
        ma=lalsim.SimInspiralCreateModeArray()
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
        lalsim.SimInspiralWaveformParamsInsertModeArray(extra_params, ma)

    hp, hc = lalsim.SimInspiralChooseFDWaveform(mass1*lal.MSUN_SI, mass2*lal.MSUN_SI, \
                             0.0, 0.0, spin1z, 0.0, 0.0, spin2z, \
                             lal.PC_SI*1e6, 0.0, 0.0, 0, 0, \
                             0, df, flow, fhigh, fref, extra_params, approximant)
    return hp, hc

    

def get_lalsim_psd_list():
    """
    Return a list of available reference PSD functions from LALSimulation.
    
    Ref: pycbc psd module
    """
    _name_prefix = 'SimNoisePSD'
    _name_suffix = 'Ptr'
    _name_blacklist = ('FromFile', 'MirrorTherm', 'Quantum', 'Seismic', 'Shot', 'SuspTherm')
    _psd_list = []
    for _name in lalsim.__dict__:
        if _name != _name_prefix and _name.startswith(_name_prefix) and not _name.endswith(_name_suffix):
            _name = _name[len(_name_prefix):]
            if _name not in _name_blacklist:
                _psd_list.append(_name)
    _psd_list = sorted(_psd_list)
    return _psd_list

_name_prefix = 'SimNoisePSD'
_name_suffix = 'Ptr'

psd_name = "aLIGOZeroDetHighPower"
noise_generator = lalsim.__dict__[_name_prefix + psd_name]
generator = np.vectorize(lambda f: 1.0 if f ==0 else noise_generator(f) )

flow = 20.0
fref = 20.0
fhigh = 1024.0
df = 0.1


length = int(fhigh/df + 1)

psd = generator( np.arange(length)*df )
ASD = np.sqrt(psd)


m1=26.009803986128812
m2=9.81919458349264 
chi = -0.7121194138041079 


params_dic = {'mtotal': m1+m2, 'eta': m1*m2/(m1+m2)**2, 'chi': chi}


print('Metric on dimensionless coordinate system \n')
print('Reference point at %s \n '%str(params_dic))
gm = metric.ComputeNumericalIMRMetric(fref, flow, fhigh, df, ASD, approximant='SEOBNRv4_ROM')
print('SEOBNRv4ROM \n', gm.Theta0Theta3Theta3s(**params_dic) )

gm = metric.ComputeNumericalIMRMetric(fref, flow, fhigh, df, ASD, approximant='SEOBNRv5_ROM')
print('SEOBNRv5ROM \n', gm.Theta0Theta3Theta3s(**params_dic) )

gm = metric.ComputeNumericalIMRMetric(fref, flow, fhigh, df, ASD, approximant='IMRPhenomD')
print('IMRPhenomD \n', gm.Theta0Theta3Theta3s(**params_dic) )

gm = metric.ComputeNumericalIMRMetric(fref, flow, fhigh, df, ASD, approximant='IMRPhenomXAS')
print('IMRPhenomXAS \n', gm.Theta0Theta3Theta3s(**params_dic) )

gm = metric.ComputeNumericalIMRMetric(fref, flow, fhigh, df, ASD, approximant='TaylorF2')
print('TaylorF2 \n', gm.Theta0Theta3Theta3s(**params_dic) )


print('\nComputation time of waveform evaluation')
npts = 1000



for approx in ['SEOBNRv4_ROM', 'SEOBNRv5_ROM', 'IMRPhenomXAS', 'IMRPhenomD', 'TaylorF2']:
    total_time = float(timeit.timeit(stmt='gen_waveform()', globals=globals(), number=npts))
    print(approx, 'approximate computation time: %.2f ms \n'%( 1000*total_time/npts) )

print('\nComputation time of metric evaluation')

npts=1000
for approx in ['SEOBNRv4_ROM', 'SEOBNRv5_ROM', 'IMRPhenomXAS', 'IMRPhenomD', 'TaylorF2']:
    total_time = float(timeit.timeit(stmt='compute_time_for_metric_computation()', globals=globals(), number=npts))
    print(approx, 'approximate computation time: %.2f ms \n'%( 1000*total_time/npts) )
